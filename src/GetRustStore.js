/*// Constants (Set these manually)
const WEBSITE_URL = "https://store.steampowered.com/itemstore/252490/browse/?filter=All"; // Change this to your target URL

// Function to fetch and clean HTML content
async function fetchAndCleanHTML(url) {
    let request = new Request(url);
    request.headers = {
        "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 14_0 like Mac OS X) AppleWebKit/537.36 (KHTML, like Gecko) Version/14.0 Mobile/15E148 Safari/537.36",
        "Cache-Control": "no-cache"
    };
    
    let responseText = await request.loadString();
    
    let webView = new WebView();
    await webView.loadHTML(responseText);
    
    let jsScript = `
        (function() {
            let parser = new DOMParser();
            let doc = parser.parseFromString(document.documentElement.outerHTML, "text/html");
            
            let targetDiv = doc.querySelector("#ItemDefsRows");
            if (targetDiv) {
                targetDiv.style.display = "flex";
                return targetDiv.outerHTML;
            } else {
                return "";
            }
        })();
    `;

    let extractedDiv = await webView.evaluateJavaScript(jsScript);

    // Construct a valid HTML page
    let completeHTML = `
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Extracted Content</title>
            <style>
                body { font-family: Arial, sans-serif; padding: 20px; }
                #ItemDefsRows { display: flex; }
            </style>
        </head>
        <body>
            ${extractedDiv ? extractedDiv : "<p>No Target Div Found</p>"}
        </body>
        </html>
    `;

    return completeHTML;
}

// Main function
async function main() {
    let htmlContent = await fetchAndCleanHTML(WEBSITE_URL);
    console.log(htmlContent);
    return htmlContent;
}

return await main();
*/



// Constants (Set these manually)
const WEBSITE_URL = "https://store.steampowered.com/itemstore/252490/browse/?filter=All"; // Change this to your target URL

// Function to fetch and clean HTML content
async function fetchAndCleanHTML(url) {
    let request = new Request(url);
    request.headers = {
        "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 14_0 like Mac OS X) AppleWebKit/537.36 (KHTML, like Gecko) Version/14.0 Mobile/15E148 Safari/537.36",
        "Cache-Control": "no-cache"
    };
    
    let responseText = await request.loadString();
    
    let webView = new WebView();
    await webView.loadHTML(responseText);
    
    let jsScript = `
        (function() {
            let parser = new DOMParser();
            let doc = parser.parseFromString(document.documentElement.outerHTML, "text/html");
            
            let targetDiv = doc.querySelector("#ItemDefsRows");
            if (targetDiv) {
                // Remove any inline styles that might interfere with our grid CSS.
                targetDiv.removeAttribute('style');
                return targetDiv.outerHTML;
            } else {
                return "";
            }
        })();
    `;
    
    let extractedDiv = await webView.evaluateJavaScript(jsScript);

    // Construct a valid HTML page with enhanced grid view styling and larger images.
    let completeHTML = `
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Extracted Content</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            padding: 20px;
            background-color: #f5f5f5;
        }
        #ItemDefsRows {
            display: grid;
            grid-template-columns: repeat(auto-fill, minmax(150px, 1fr));
            gap: 10px;
            margin: 0 auto;
        }
        #ItemDefsRows > div {
            background: #fff;
            border: 1px solid #ddd;
            padding: 10px;
            border-radius: 4px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }
        #ItemDefsRows img {
            width: 100%;
            height: auto;
            object-fit: cover;
        }
    </style>
</head>
<body>
    ${extractedDiv ? extractedDiv : "<p>No Target Div Found</p>"}
</body>
</html>
    `;

    return completeHTML;
}

// Main function
async function main() {
    let htmlContent = await fetchAndCleanHTML(WEBSITE_URL);
    console.log(htmlContent);
    return htmlContent;
}

return await main();
