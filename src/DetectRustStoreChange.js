// Constants (Set these manually)
const WEBSITE_URL = "https://store.steampowered.com/itemstore/252490/browse/?filter=All"; // Change this to your target URL
const WEBSITE_NAME = "rustStore";  // Change this to a unique identifier for caching

// Cache directory in Scriptable
const CACHE_FILE = `cache/cache_${WEBSITE_NAME}.txt`;

// Function to fetch and clean HTML content
async function fetchAndCleanHTML(url) {
    let request = new Request(url);
    
    let responseText = await request.loadString();
    
    let webView = new WebView();
    await webView.loadHTML(responseText);
    
    let jsScript = `
        (function() {
            let parser = new DOMParser();
            let doc = parser.parseFromString(document.documentElement.outerHTML, "text/html");
            
            let targetDiv = doc.querySelector("#ItemDefsRows");
            if (targetDiv) {
                targetDiv.style.display = "flex";
                return targetDiv.outerHTML;
            } else {
                return "<html><body>No Target Div Found</body></html>";
            }
        })();
    `;

    let cleanedHTML = await webView.evaluateJavaScript(jsScript);
    return cleanedHTML;
}

// Function to compare with cached version
async function hasWebsiteChanged() {
    let htmlContent = await fetchAndCleanHTML(WEBSITE_URL);
    
    let fileManager = FileManager.iCloud();
    let cachePath = fileManager.joinPath(fileManager.documentsDirectory(), CACHE_FILE);
    
    if (!fileManager.fileExists(cachePath)) {
        fileManager.writeString(cachePath, htmlContent);
        return 0; // First time fetching, no previous data
    }
    
    let previousContent = fileManager.readString(cachePath);
    if (similarity(htmlContent, previousContent) > 0.9) {
        return 0; // No significant change
    } else {
        fileManager.writeString(cachePath, htmlContent);
        return 1; // Website changed
    }
}

// Function to calculate similarity between two strings
function similarity(str1, str2) {
    let longer = str1.length > str2.length ? str1 : str2;
    let shorter = str1.length > str2.length ? str2 : str1;
    let matchCount = 0;

    for (let i = 0; i < shorter.length; i++) {
        if (shorter[i] === longer[i]) matchCount++;
    }

    return matchCount / longer.length;
}

// Main function
async function main() {
    let status = await hasWebsiteChanged();

    if (status === -1) {
        console.log("Error fetching website");
        return "Error fetching website";
    } else if (status === 0) {
        console.log("Website is the same");
        return "No changes detected";
    } else if (status === 1) {
        console.log("Website has changed!");
        return "Website has changed";
    }
}

return await main();
