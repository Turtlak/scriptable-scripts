// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: deep-gray; icon-glyph: magic;

// Change these to your usernames!
const user = 'MyFunnyNickname';
const deviceName = Device.name();


const DEFAULT_LOCATION = {
  latitude: 0,
  longitude: 0
};

/******/

const data = {};
const widget = await createWidget(data);
Script.setWidget(widget);
Script.complete();
widget.presentMedium();

async function createWidget(data) {
  console.log(data)
  const w = new ListWidget()
  const bgColor = new LinearGradient()
  bgColor.colors = [new Color("#000000"), new Color("#000000")]
  bgColor.locations = [0.0, 1.0]
  w.backgroundGradient = bgColor
  w.setPadding(12, 15, 15, 12)

  const stack = w.addStack();
  stack.layoutHorizontally();

  const leftStack = stack.addStack();
  leftStack.layoutVertically();
  leftStack.spacing = 6;
  leftStack.size = new Size(200, 0);

  //Location.setAccuracyToThreeKilometers();
  //const location = await Location.current();
  //const address = await Location.reverseGeocode(location.latitude, location.longitude);

  const today = new Date;
  const dateFormater = new DateFormatter;
  dateFormater.useMediumDateStyle();

  const LastLoginLine = addLine(leftStack.addText(`Last login: ${Date.now()}`));
  const userLine = addLine(leftStack.addText(`${user}@${deviceName}:~ $`));

  userLine.textOpacity = 0.7

  
  const batteryLine = addLine(leftStack.addText(` ${renderBattery()}`));
  
  //const locationLine = addLine(leftStack.addText(` Location: ${address[0].locality}`))
  
  stack.addSpacer();
  const rightStack = stack.addStack();
  rightStack.spacing = 2;
  rightStack.layoutVertically();
  rightStack.bottomAlignContent();

  return w
}

function addLine(line, textColor = null, font = null){
    line.textColor = textColor ?? new Color("#7dbbae")
    line.font = font ?? new Font("Menlo", 11)
    return line;
}

function renderBattery() {
  const batteryLevel = Device.batteryLevel()
  const juice = "#".repeat(Math.floor(batteryLevel * 8))
  const used = "-".repeat(8 - juice.length)
  const batteryAscii = `[${juice}${used}] ${Math.round(batteryLevel * 100)}%`
  return batteryAscii
}



async function fetchJson(key, url, headers) {
  const cached = await cache.read(key, 5);
  if (cached) {
    return cached;
  }

  try {
    console.log(`Fetching url: ${url}`);
    const req = new Request(url);
    req.headers = headers;
    const resp = await req.loadJSON();
    cache.write(key, resp);
    return resp;
  } catch (error) {
    try {
      return cache.read(key, 5);
    } catch (error) {
      console.log(`Couldn't fetch ${url}`);
    }
  }
}

